#!/bin/bash


yad_args=(
	--form
        --title "Gerador de Senhas"
        --field="Tamanho da Senha":NUM
        --field="Letras Maiúsculas":CHK
        --field="Letras Minúsculas":CHK
        --field="Números":CHK
        --field="Caracteres Especiais":CHK
	)

input=$(yad "${yad_args[@]}")
IFS='|' read -r tam uppercase lowercase numbers specials <<< "$input"

allowed_chars=""
[[ "$uppercase" == TRUE ]] && allowed_carac+="ABCDEFGHIJKLMNOPQRSTUVWXZ"
[[ "$lowercase" == TRUE ]] && allowed_carac+="abcdefghijklmnopqrstuvwxyz"
[[ "$numbers" == TRUE ]] && allowed_carac+="0123456789"
[[ "$specials" == TRUE ]] && allowed_carac+="!@#-+%?*()"

if [[ -z "$allowed_carac" ]]; then
       yad --error --text "selecione pelo menos uma opção de caracteres."
       exit 1
fi

gerador_de_senhas() {
        tam=$1
        carac=$2
        sen=""
	for i in $(seq 1 $tam); do
                        senha="$(echo "$carac" | fold -w1 | shuf |  head -n1)"
                        sen="${sen}${senha}"
                done
                echo "$sen"
}
	sen1=$(gerador_de_senhas "$tam" "$allowed_carac")
	sen2=$(gerador_de_senhas "$tam" "$allowed_carac")
	sen3=$(gerador_de_senhas "$tam" "$allowed_carac")
	yad --title "Opçoes de Senha" --info --text "Opçoes de senhas geradas:\n\n1.$sen1\n2. $sen2\n3. $sen3" --button="OK"
